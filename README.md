# OpenML dataset: GLI

https://www.openml.org/d/45089

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Gliomas (GLI, GLI-85) dataset**

**Authors**: W. Freije, F. Castro-Vargas, Z. Fang, S. Horvath, T. Cloughesy, L. Liau, P. Mischel, S. Nelson

**Please cite**: ([URL](https://www.scopus.com/record/display.uri?eid=2-s2.0-4644313275&origin=inward)): W. Freije, F. Castro-Vargas, Z. Fang, S. Horvath, T. Cloughesy, L. Liau, P. Mischel, S. Nelson, Gene expression profiling of gliomas strongly predicts survival, Cancer Res. 64 (18) (2004) 6503-6510

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45089) of an [OpenML dataset](https://www.openml.org/d/45089). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45089/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45089/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45089/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

